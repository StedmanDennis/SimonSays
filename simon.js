'use strict'

Vue.component('tile',{
	props: ['tile','answerMode','level','animationLock'],
	template: 	`
					<div v-if='!tile.active' v-bind:style='"color: "+tile.color' v-bind:class='"tile"+tile.id' class='tile centeredText basicBorder' v-on:click='clickOn()'>
						{{tile.color}}({{tile.id}})
					</div>
					<div v-else v-bind:style='"background-color: "+tile.color' v-bind:class='"tile"+tile.id' class='basicBorder'></div>
				`,
	data: function(){
		return {

		}
	},
	methods:{
		clickOn: function(){
			if (this.answerMode && this.level > 0 && !this.animationLock){
				this.$emit("lockAnimation");
				this.tile.active = true;
				setTimeout(()=>{
					this.tile.active = false;
					this.$emit('unlockAnimation');
					this.$emit('check', this.tile.id);
				}, 500);
			}
		}
	}
})

Vue.component('tracker',{
	props: ['level','answerTrack','answerMode'],
	template: 	`
					<div v-if='this.level < 1' class='tracker centeredText' v-on:click='$emit("start")'>
						Press here to start
					</div>
					<div v-else class='tracker centeredText'>
						On Level: {{level}}
						<p v-if='answerMode'>At {{answerTrack}}/{{level}}</p>
						<p v-else>Watch Closely...</p>
					</div>
				`,
})

Vue.component('gameInstance',{
	template: 	`
					<div class='gameContainer'>
						<tile v-for='tile in tileData' v-bind:key=tile.id v-bind:tile=tile v-bind:animationLock=animationLock v-bind:answerMode=answerMode v-bind:level=level v-on:lockAnimation='lockAnimation' v-on:unlockAnimation='unlockAnimation' v-on:check='checkClick($event)'></tile>
						<tracker v-bind:level=level v-bind:answerTrack=answerTrack v-bind:answerMode=answerMode v-on:start='nextLevel'></tracker>
					</div>
				`,
	data: function(){
		return {
			tileData: [
				{
					id: 0,
					color: "green",
					active: false
				},
				{
					id: 1,
					color: "red",
					active: false
				},
				{
					id: 2,
					color: "yellow",
					active: false
				},
				{
					id: 3,
					color: "blue",
					active: false
				}
			],
			level: 0,
			answerMode: false,
			answerTrack: 0,
			animationLock: false,
			solution: []
		}
	},
	methods: {
		generateProblem: function(){
			this.solution = [];
			this.answerTrack = 0;
			for (let sub = 0; sub < this.level; sub++){
				this.solution.push(this.generateRandomBetween(0,3));
			}
		},
		generateRandomBetween: function(min, max){
			return Math.floor(Math.random() * (max - min + 1) ) + min;;
		},
		highlightSolution: function(){
			this.answerMode = false;
			for (let test = 0; test < this.solution.length; test++){
				setTimeout(()=>{
					this.tileData[this.solution[test]].active = true;
				}, 1000 + (2000 * test));
				setTimeout(()=>{
					this.tileData[this.solution[test]].active = false;
				}, 2000 + (2000 * test));						
			}
			setTimeout(()=>{
				this.answerMode = true;
				console.log('Complete Solution: ' + this.solution);
			}, 2000 + (2000 * (this.solution.length-1)));	
		},
		nextLevel: function(){
			this.level+=1;
			this.generateProblem();
			this.highlightSolution();
		},
		lockAnimation: function(){
			//console.log('locking');
			this.animationLock = true;
		},
		unlockAnimation: function(){
			//console.log('unlocking');
			this.animationLock = false;
		},
		checkClick: function(payload){
			if (this.solution[this.answerTrack] == payload){
				console.log('correct');
				this.answerTrack++;
				if (this.answerTrack < this.solution.length){
					console.log('Next answer: ' + this.solution[this.answerTrack]);
				}
			}else{
				console.log('wrong');
				this.level = 0;
			}
			if (this.answerTrack == this.solution.length){
				this.nextLevel();
			}
		}
	}
})

const eventBus = new Vue({}) //global event bus

new Vue({
	el: '#vue-app',
	template:	`
					<div class='mainContainer basicBorder'>
						<gameInstance></gameInstance>
					</div>	
				`,
	data: {
		
	},
	methods: {
		generateRandomBetween: function(min, max){
			return Math.floor(Math.random() * (max - min));
		}
	}
})